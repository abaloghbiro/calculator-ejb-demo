package hu.braininghub.bh06.calculator.service;

import javax.ejb.Stateless;

@Stateless
public class DefaultCalculator implements Calculator {
	
	
	public Double add(Double a, Double b) {
		return a + b;
	}
}
