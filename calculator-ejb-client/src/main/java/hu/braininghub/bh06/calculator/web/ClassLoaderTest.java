package hu.braininghub.bh06.calculator.web;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ClassLoaderTest {

	public static class Bh06ClassLoader extends ClassLoader {

		@Override
		public Class<?> findClass(String name) {
			byte[] bt = loadClassData(name);
			return defineClass(name, bt, 0, bt.length);
		}

		private byte[] loadClassData(String className) {
			// read class
			InputStream is = getClass().getClassLoader().getResourceAsStream(className.replace(".", "/") + ".class");
			ByteArrayOutputStream byteSt = new ByteArrayOutputStream();
			// write into byte
			int len = 0;
			try {
				while ((len = is.read()) != -1) {
					byteSt.write(len);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			// convert into byte array
			return byteSt.toByteArray();
		}

	}

	public static void main(String[] args)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException {

		DefaultService s1 = new DefaultService();

		Class<?> serviceClass = new Bh06ClassLoader()
				.findClass("hu.braininghub.bh06.calculator.web.DefaultService");

		
		System.out.println(s1.getClass().getClassLoader());
		System.out.println(serviceClass.getClassLoader());
		
		@SuppressWarnings("deprecation")
		DefaultService s2 = (DefaultService) serviceClass.newInstance();


	}
}
