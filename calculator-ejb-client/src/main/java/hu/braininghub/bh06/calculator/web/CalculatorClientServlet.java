package hu.braininghub.bh06.calculator.web;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hu.braininghub.bh06.calculator.service.CalculatorLocal;

@WebServlet(urlPatterns = "/client")
public class CalculatorClientServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	@EJB(lookup="java:global/calculator/DefaultCalculator!hu.braininghub.bh06.calculator.service.CalculatorLocal")
	private CalculatorLocal calc;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.getWriter().print("<h1><marquee>" + calc.add(3.0, 3.4) + "</marquee></h1>");
	}

}
