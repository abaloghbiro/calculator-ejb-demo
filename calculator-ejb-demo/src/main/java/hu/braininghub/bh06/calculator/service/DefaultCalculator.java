package hu.braininghub.bh06.calculator.service;

import java.sql.PreparedStatement;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.sql.DataSource;
import javax.transaction.UserTransaction;

@Stateless
@Local(CalculatorLocal.class)
@Remote(CalculatorRemote.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class DefaultCalculator implements CalculatorRemote, CalculatorLocal {

	@Resource(lookup = "jdbc/oracle")
	private DataSource ds;

	@Resource
	private UserTransaction ut;

	public Double add(Double a, Double b) {

		Double ret = null;

		try (PreparedStatement stmt = ds.getConnection().prepareStatement("INSERT INTO CALCRES (RESULT) VALUES (?)")) {
			ut.begin();
			ret = a + b;
			stmt.setDouble(1, ret);
			stmt.executeUpdate();
			ut.commit();
		} catch (Exception e) {
			try {
				ut.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}

		return ret;
	}
}
