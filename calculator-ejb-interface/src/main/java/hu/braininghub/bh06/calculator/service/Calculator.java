package hu.braininghub.bh06.calculator.service;

public interface Calculator {

	Double add(Double a, Double b);
}
